# Data Prevention

Data Prevention is a plugin for Firefox, it illustrates the underlying process of a webpage like instagram. It puts in front of the website the multiple request that are hiding behind the fancy front end.

I did this plugin for my master's degree, it is not useful to navigate online, but it show all the process that you do not see when you open and scroll a webpage with strong form until you can't see anything of what you wanted to see.



![28.png](assets/de267025c51b062f3362a3634ef23baacb2488d9.png)



![31.png](assets/dee02dd2e1c36943f03f941140bd84d8f632c31f.png)
