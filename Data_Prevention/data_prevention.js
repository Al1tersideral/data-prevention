//Création des éléments div du plugin
let div = document.createElement("div");
let pRequest = document.createElement("p");
let pSize = document.createElement("p");
let warning = document.createElement("div");
let warning2 = document.createElement("div");
let warning3 = document.createElement("div");
let warning4 = document.createElement("div");
div.setAttribute("class", "dataprevention");
let infos = document.createElement("div");
let ig = browser.runtime.getURL("img/101.png");
let ring = browser.runtime.getURL("img/Ring.png");




//Création d'une variable qui importe une fonte dans le dossier du plugin dans le site web
let font = browser.runtime.getURL("fonts/LightWave.woff");
var style = document.createElement('style');
style.innerHTML =  "@font-face { font-family: 'LightWave'; src: url('" + font + "') format('woff'); font-weight: normal; font-style: normal; }"



//Insérer le style css pour la fontface
document.body.append(style);

//Insérer l'élément HTML pour mettre les informations du performance observer
document.body.append(div);
div.append(pRequest);
div.append(pSize);
document.body.append(warning);
document.body.append(warning2);
document.body.append(warning3);
document.body.append(warning4);




//Function scale up fontsize
function ScaleUp(nombre, tailleMax){

    // Définir la taille de la police initiale
    let taillePolice = 1;
    let letterspace = 0;
    let lineheight = 0;

    // Augmenter la taille de la police en fonction du nombre
    taillePolice += nombre/500;

    letterspace = taillePolice/7;

    lineheight = taillePolice*1.2;
  
    // Limiter la taille de la police à la taille maximale
    if (taillePolice > tailleMax) {
      taillePolice = tailleMax;
    }
    if (letterspace > -1){
      letterspace = -1.5;
    }
    if (lineheight>11){
      lineheight = 11;
    }
    div.style.fontSize = `${taillePolice}rem`;
    div.style.letterSpacing = `-${letterspace}rem`;
    div.style.lineHeight = `${lineheight}rem`;
}

//Function de clignotemment à partir d'un certain seuil
function clignote(nombre, element) {
  // Vérifier si le nombre est supérieur ou égal à la valeur seuil
  if(nombre >= 2000){
    let intervalIdLow;
    let visible = true;

    // Fonction pour changer la visibilité de l'élément
    function toggleVisibiliteLow() {
      if (visible) {
        element.style.opacity = '0';
      } else {
        element.style.opacity = '1';
      }
      visible = !visible;
    }

  }

  
  intervalIdLow = setInterval(toggleVisibiliteLow, 3000); // Clignoter toutes les 500 millisecondes
  
  if (nombre >= 5000) {
    let intervalId;
    let visible = true;

    // Fonction pour changer la visibilité de l'élément
    function toggleVisibilite() {
      if (visible) {
        element.style.opacity = '0';
      } else {
        element.style.opacity = '1';
      }
      visible = !visible;
    }

    // Démarrer l'intervalle de clignotement
    intervalId = setInterval(toggleVisibilite, 100); // Clignoter toutes les 500 millisecondes
  }

  if (nombre >= 7000){
    // Fonction pour changer la visibilité de l'élément
        element.style.opacity = '1';
  }
}

// Création du conteneur de liste
var conteneur = document.createElement("div");
document.body.appendChild(conteneur);

// Création de la liste
var liste = document.createElement("ul");
liste.id = "maListe";
conteneur.appendChild(liste);
// Fonction pour ajouter un élément à la liste
function ajouterElement(content) {
  var nouvelElement = document.createElement("li");
  nouvelElement.innerText = content;
  liste.appendChild(nouvelElement);

  setTimeout(function() {
    liste.removeChild(nouvelElement);
  }, 5000); // Disparaître après 5 secondes (5000 millisecondes)
}


//Div Style implementation
div.style.position = "fixed";
div.style.display = "flex";
div.style.width ="100%";
div.style.height = "100%";
div.style.bottom = "0";
div.style.left ="0";
div.style.margin = "0";
div.style.zIndex = "1000";
div.style.fontFamily = "LightWave";
div.style.pointerEvents = "none";
div.style.mixBlendMode = "difference";
div.style.color="white";
div.style.padding = "0";
div.style.margin = "0";
div.style.opacity = "0";
div.style.transitionDuration ="0.2s";

//Informations style implementation
infos.style.position = "fixed";
infos.style.dysplay ="block";
infos.style.left ="50%";
infos.style.bottom="0";
infos.style.padding ="0.5rem";
infos.style.fontFamily = "LightWave";
infos.style.fontSize ="5rem";
infos.style.lineHeight ="15rem";
infos.style.opacity = "1";
infos.style.transitionDuration ="1s";
infos.style.zIndex = "20";

//pSize element style
pSize.style.position ="absolute";
pSize.style.right = "1%";
pSize.style.bottom = "0";
pSize.style.margin = "0";
pSize.style.zIndex = "20";
// pSize.style.color = getRandomColor();




conteneur.style.position = "fixed";
conteneur.style.width = "14vw";
conteneur.style.height = "100vh";
conteneur.style.top = "0";
conteneur.style.right = "0";
conteneur.style.zIndex = "10";
conteneur.style.wordBreak = "auto";
conteneur.style.fontFamily = "LightWave"
conteneur.style.fontSize = "2rem";
conteneur.style.lineHeight = "2rem";

warning.style.position = "fixed";
warning.style.fontSize = "100rem";
warning.style.fontFamily = "LightWave"
warning.style.pointerEvents = "none";
warning.style.mixBlendMode = "difference";
warning.style.right = "15%";
warning.style.top = "30%";
warning.style.opacity = "0";
warning.style.zIndex = "20";
warning.innerHTML = "+";

warning2.style.position = "fixed";
warning2.style.fontSize = "300rem";
warning2.style.fontFamily = "LightWave"
warning2.style.pointerEvents = "none";
warning2.style.mixBlendMode = "difference";
warning2.style.left = "70%";
warning2.style.top = "85%";
warning2.style.opacity = "0";
warning2.style.zIndex = "20";
warning2.innerHTML = "-";

warning3.style.position = "fixed";
warning3.style.fontSize = "100rem";
warning3.style.fontFamily = "LightWave"
warning3.style.pointerEvents = "none";
warning3.style.mixBlendMode = "difference";
warning3.style.left = "-10%";
warning3.style.top = "20%";
warning3.style.opacity = "0";
warning3.style.zIndex = "20";
warning3.innerHTML = "<img src="+ring+">";

warning4.style.position = "fixed";
warning4.style.pointerEvents = "none";
warning4.style.left = "0";
warning4.style.top = "0";
warning4.style.width = "100vw";
warning4.style.height = "100vh";
warning4.style.opacity = "0";
warning4.style.zIndex = "20";
warning4.innerHTML = "<img src="+ig+">";





//pRequest element style
pRequest.style.position = "absolute";
pRequest.style.left ="1%";
pRequest.style.top = "1%";
pRequest.style.margin = "0";
pRequest.style.lineHeight = "12rem";


// Création d'un objet PerformanceObserver
let requests;
let json;
let script;
let xhrs;
let fetchye;
let requestTimer;
let images;

let totalBytes = 0;


const observer = new PerformanceObserver((list) => {
  const entries = list.getEntries();

  

  requests += entries.filter((entry) => entry.entryType === 'resource'&&
  entry.initiatorType !== 'img');
  json += entries.filter((entry) => entry.initiatorType === 'link');
  script += entries.filter((entry)=> entry.initiatorType === 'other');
  xhrs += entries.filter(entry => entry.initiatorType === 'xmlhttprequest');
  fetchye += entries.filter((entry)=> {
    ajouterElement(entry.initiatorType);
  entry.entryType === 'fetch'});
  images += entries.filter((entry) => entry.initiatorType === 'img');

 
  // console.log(`Le nombre de requêtes effectuées est de: ${requests.length}`);


  if(requests.length > 2000){
    infos.style.opacity = "1";
    div.style.opacity = "1";
  }

  //récupérer la quantité de données transférer
  entries.forEach((entry) => {
    if (entry.entryType === 'resource') {
      totalBytes += entry.transferSize || 0;
    }
  });

  //Lance les animations
  ScaleUp(requests.length, 9);
  clignote(requests.length, div);
  


  const totalMegabytes = (totalBytes / (1024 * 1024)).toFixed(2);

  let totalMegaOctets = totalMegabytes * 0.125 ;

  pRequest.innerHTML = `${requests.length} REQUESTS</br>${json.length} LINKS </br>${script.length} OTHERS </br>${xhrs.length} XHRS </br> ${images.length} IMAGES`;
  pSize.innerHTML = `${totalMegaOctets}Mo`

  if(requests.length>2000){
    warning.style.opacity = "1";
  }
  if(requests.length>3000){
    warning2.style.opacity = "1";
  }
  if(requests.length>4000){
    warning3.style.opacity = "1";
  }
  if(requests.length>5000){
    warning4.style.opacity = "1";
  }

  if(requests.length>6000){
    document.body.remove();
    //Insérer le style css pour la fontface


    //Insérer l'élément HTML pour mettre les informations du performance observer
      // Créez un nouvel élément body
    var newBody = document.createElement('body');

      // Ajoutez des attributs ou des classes au nouvel élément body si nécessaire

      // Ajoutez le nouvel élément body au document
    document.documentElement.appendChild(newBody);
    infos.style.opacity = "1";
    div.style.opacity = "1";

    div.append(warning);
    div.append(warning2);
    div.append(warning3);
    div.append(warning4);
    document.body.append(style);
    document.body.append(div);
    div.append(pRequest);
    div.append(pSize);
  }
  
});



// Démarrage de l'observation des performances
observer.observe({entryTypes: ['resource']});







